class Fixnum

  def in_words(n=self) #77
    num_words = {1000000000000 => "trillion", 1000000000 => "billion", 1000000 => "million", 1000 => "thousand", 100 => "hundred", 90 => "ninety", 80 => "eighty", 70 => "seventy", 60 => "sixty", 50 => "fifty", 40 => "forty",
      30 => "thirty", 20 => "twenty", 19 =>"nineteen", 18 =>"eighteen", 17 =>"seventeen", 16 =>"sixteen", 15 =>"fifteen", 14 =>"fourteen",
      13 =>"thirteen", 12 =>"twelve", 11 => "eleven", 10 => "ten", 9 => "nine", 8 => "eight", 7 => "seven", 6 => "six", 5 => "five", 4 => "four",
      3 => "three", 2 => "two", 1 => "one"}
    word_str = ''

    # return 'zero' if self == 0

    num_words.each do |num, name|

      if n < 1
        return 'zero'
      elsif n.to_s.length == 1 && n / num > 0
        return word_str + "#{name}"
      elsif n < 100 && n / num > 0
        if n % num == 0
          return word_str + "#{name}"
        else
         ones_num = in_words(n % num)
         return remove_zero(word_str + "#{name} " + ones_num)
        end
      elsif  n / num > 0
        return remove_zero(word_str + in_words(n / num) + " #{name} " + in_words(n % num))
      end
    end
  end
  private

  def remove_zero(str)
    str_array = str.split(' ')
    if str_array.last == 'zero'
      str_array.pop
    end
    str_array.join(' ')
  end
end
